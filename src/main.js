var colors = {
  light: [13, 13, 13],
  dark: [0, 0, 0]
}

var boardSize
var inc = 0.05
var noiseMax = 8
var zoff = 0
var steps = 25

function setup() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}

function draw() {
  createCanvas(windowWidth, windowHeight)
  background(colors.light)
  rectMode(CENTER)
  colorMode(RGB, 255, 255, 255, 1)

  fill(colors.dark)
  noStroke()
  rect(windowWidth *  0.5, windowHeight * 0.5, boardSize, boardSize)

  push()
  translate(windowWidth * 0.5, windowHeight * 0.5)
  for (var j = 0; j < steps; j++) {
    stroke(255)
    noStroke()
    fill(128 + 128 * ((j + 7) / steps) * sin(frameCount * 0.125 + j * 0.5))
    beginShape()
    for (var i = 0; i <= Math.PI * 2; i+= inc) {
      var xoff = map(cos(i), -1, 1, 0, noiseMax)
      var yoff = map(sin(i), -1, 1, 0, noiseMax)
      var r = map(noise(xoff, yoff, zoff), 0, 1, boardSize * (0.45 - j * (1 / steps * 0.3)) + boardSize * 0.025 * sin(frameCount * 0.1 + j * (3.5 + 0.2 * cos(frameCount * 0.05))), boardSize * (0.35 - j * (1 / steps * 0.3)) + boardSize * 0.025 * sin(frameCount * 0.1 + j * (3.5 + 0.2 * cos(frameCount * 0.05))))
      var x = r * cos(i)
      var y = r * sin(i)
      vertex(x, y)
    }
    endShape(CLOSE)
  }
  pop()

  zoff += 0.025
}

function windowResized() {
  if (windowWidth >= windowHeight) {
    boardSize = windowHeight - 80
  } else {
    boardSize = windowWidth - 80
  }
  createCanvas(windowWidth, windowHeight)
}
